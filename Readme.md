# Project Name Automation.Api&Ui.Tests

## Overview
This project is a test automation framework using Behavior-Driven Development (BDD) with SpecFlow and RestSharp in Visual Studio.

## Features
- BDD testing with SpecFlow
- Integration with RestSharp for API testing
- Organized project structure for maintainability
- Allure report
- Version control with Git

## Tools and Framework used
- Visual Studio
- .NET Framework
- SpecFlow for Visual Studio
- RestSharp
-Allure

## To Start
1. Clone this repository.
2. Open the solution in Visual Studio.
3. Install required packages via NuGet.
4. Build the solution.

## Usage
- Used SpecFlow feature files to write test scenarios in Gherkin syntax.
- Implement step definitions in C# to execute the scenarios.
- Utilize RestSharp for API testing within the step definitions.
- Run tests using Visual Studio test explorer or command line.

## Folder Structure
- Features: Contains SpecFlow feature files.
- StepDefinitions: Holds C# code for step definitions.
- Models for posting request.
- Pages folder for containing pages
- Different Step files for Api and Ui
- One Hooks file for both initialization (Api & Ui)
- Setup file for client steup for api.

