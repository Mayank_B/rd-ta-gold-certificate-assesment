﻿using Automation.GC.Tests.Model;
using Automation.GC.Tests.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.GC.Tests.StepDefinitions
{
    [Binding]
    public class UiStepsDefinition : StepsBaseUI
    {
        private readonly ScenarioContext _scenarioContext;
        private readonly LoginValidationPage _loginValidationPage;
        private readonly By _userName = By.XPath("//label[@id='userName-value']");
        private readonly ApiStepsDefinition apiStepsDefinition;

        public UiStepsDefinition(ScenarioContext scenarioContext) : base(scenarioContext)
        {
            _scenarioContext = scenarioContext;
            _loginValidationPage = new LoginValidationPage(driver);
        }

        [When(@"I navigate to url")]
        public void WhenINavigateToUrl()
        {
            driver.Navigate().GoToUrl("https://demoqa.com/login");
        }

        [When(@"I should enter username and password")]
        public void WhenIShouldEnterUsernameAndPassword()
        {
            _loginValidationPage.UserLogin();
        }

        [Then(@"I should click on login button")]
        public void ThenIShouldClickOnLoginButton()
        {
            _loginValidationPage.ClickOnLoginButton();
        }

        [Then(@"I should validate the username in dashboard page")]
        public void ThenIShouldValidateTheUsernameInDashboardPage()
        {

            Assert.That(driver.FindElement(_userName).Text, Is.EqualTo("passtestdatauser"));
        }

        [Given(@"I launch the URL on WEB UI")]
        public void GivenILaunchTheURLOnWEBUI()
        {
            driver.Navigate().GoToUrl("https://demoqa.com/books");
        }


        [Then(@"I should validate if the Title, Author, Publisher match with the API response")]
        public void ThenIShouldValidateIfTheTitleAuthorPublisherMatchWithTheAPIResponse()
        {
            var bookElements = driver.FindElements(By.XPath("//div[@class='rt-tbody']"));
            var uiBooks = new List<Book>();

            for (int i = 0; i < bookElements.Count; i++)
            {
                var title = driver.FindElement(By.XPath($"//a[normalize-space()]")).Text;
                var author = driver.FindElement(By.XPath($"//div[normalize-space()]")).Text;
                var publisher = driver.FindElement(By.XPath($"//div[@class='rt-tbody']//div[{i + 1}]//div[1]//div[4]")).Text;

                uiBooks.Add(new Book
                {
                    title = title,
                    author = author,
                    publisher = publisher
                });
            }

            var apiBooks = _scenarioContext.Get<List<Book>>("ApiBooks");

            Assert.AreEqual(apiBooks.Count, uiBooks.Count, "Count of books from API and UI does not match");

            for (int i = 0; i < apiBooks.Count; i++)
            {
                Assert.AreEqual(apiBooks[i].title, uiBooks[i].title);
                Assert.AreEqual(apiBooks[i].author, uiBooks[i].author);
                Assert.AreEqual(apiBooks[i].publisher, uiBooks[i].publisher);
            }
        }

    }
}
