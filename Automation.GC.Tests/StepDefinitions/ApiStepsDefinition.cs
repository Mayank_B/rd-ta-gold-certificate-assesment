﻿using Automation.GC.Tests.Model;
using Newtonsoft.Json;
using NUnit.Framework;
using OpenQA.Selenium;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.GC.Tests.StepDefinitions
{
    [Binding]
    public class ApiStepsDefinition : StepsBaseApi
    {
        private readonly RestClient _client;
        private readonly ApiOperations _apiOperations;
        private readonly ScenarioContext _scenarioContext;
        private RestResponse _response;
        private List<Book> _books;
        private IWebDriver _driver;
        public Book _booksList;
        private readonly By _authorName = By.XPath("//div[contains(text(),'Author')]");

        public ApiStepsDefinition(ScenarioContext _scenarioContext) : base(_scenarioContext)
        {
            _client = _scenarioContext.Get<RestClient>("Client");
            _apiOperations = new ApiOperations(_client);
        }

        [Given(@"I should create a new user")]
        public void GivenIShouldCreateANewUser()
        {
            var postRequest = new PostRequest { userName = "passtestdatauser", password = "Test@1290" };
            _response = _apiOperations.Post("Account/v1/User", postRequest);
        }

        [Then(@"I should validate the status code")]
        public void ThenIShouldValidateTheStatusCode()
        {
            Assert.AreEqual(201, (int)_response.StatusCode);
        }

        [Then(@"I should validate the responsebody")]
        public void ThenIShouldValidateTheResponsebody()
        {
            _apiOperations.ValidateResponseBody();
        }

        [Given(@"I have made a GET call to retrieve book")]
        public void GivenIHaveMadeAGETCallToRetrieveBookDetails()
        {
            _apiOperations.Get("BookStore/v1/Books");
        }

        [Then(@"I should store the details of the books and verify with ui")]
        public void ThenIShouldStoreTheDetailsOfTheBooks()
        {
            var response = _apiOperations.Get("BookStore/v1/Books");
            _booksList = JsonConvert.DeserializeObject<Book>(response.Content);
           // _scenarioContext.Add("ApiBooks", _booksList);
        }
    }
}
