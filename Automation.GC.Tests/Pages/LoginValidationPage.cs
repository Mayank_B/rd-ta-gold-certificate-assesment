﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.GC.Tests.Pages
{
    public class LoginValidationPage
    {
        private readonly IWebDriver driver;

        private readonly By _userName = By.XPath("//input[@id='userName']");
        private readonly By _passWord = By.XPath("//input[@id='password']");
        private readonly By _loginButton = By.XPath("//button[@id='login']");

        public LoginValidationPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void UserLogin()
        {
            IWebElement UserName = driver.FindElement(_userName);
            UserName.SendKeys("passtestdatauser");

            IWebElement Password = driver.FindElement(_passWord);
            Password.SendKeys("Test@1290");
        }

        public void ClickOnLoginButton()
        {
            IWebElement LoginButton = driver.FindElement(_loginButton);
            LoginButton.Click();
            Thread.Sleep(3000);
            //WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        }
    }
}
