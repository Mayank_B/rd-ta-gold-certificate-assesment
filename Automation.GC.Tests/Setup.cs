﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.GC.Tests
{
    public class Setup
    {
        public static RestClient ClientInit()
        {
            string url = "https://demoqa.com/";

            var restClient = new RestClient(url);
            return restClient;
        }
    }
}
