﻿Feature: Login and validating

I am creating the new user and performing the validation

@ApiTesting
Scenario: LoginValidationApi
	Given I should create a new user
	Then I should validate the status code
	Then I should validate the responsebody

@UiTesting
Scenario: LoginValidationUi
	When I navigate to url
	And I should enter username and password
	Then I should click on login button
	Then I should validate the username in dashboard page



@ApiTesting
Scenario: GetBooksDetails
    Given I have made a GET call to retrieve book
    Then I should store the details of the books and verify with ui
