﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.GC.Tests
{
    public class Driver
    {
        public static IWebDriver driverInstance;

        public Driver() { }

        public IWebDriver GetDriver(string browserName)
        {
            if (driverInstance == null)
            {
                switch (browserName.ToLower())
                {
                    case "chrome":
                        driverInstance = new ChromeDriver();
                        break;
                    case "firefox":
                        driverInstance = new FirefoxDriver(); // Not installed
                        break;
                    case "ie":
                        driverInstance = new InternetExplorerDriver(); // Not installed
                        break;
                    default:
                        throw new ArgumentException("Invalid browser name specified.");
                }
            }
            return driverInstance;
        }

        public static void QuitDriver()
        {
            driverInstance.Quit();
            driverInstance.Dispose();
            driverInstance = null;
        }
    }
}
