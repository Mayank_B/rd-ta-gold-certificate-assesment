﻿using Automation.GC.Tests.Model;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.GC.Tests
{
    public class ApiOperations
    {
        private readonly RestClient client;
        private RestResponse _apiResponse;

        public ApiOperations(RestClient client)
        {
            this.client = client;
        }

        public RestResponse Post<T>(string resource, T t) where T : class
        {
            RestRequest request = new RestRequest(resource, Method.Post);
            request.AddJsonBody(t);
            _apiResponse = (RestResponse)client.Execute(request);
            return _apiResponse;
        }

        public void ValidateResponseBody()
        {
            var responseContent = _apiResponse.Content;
            var responseBody = Newtonsoft.Json.JsonConvert.DeserializeObject<PostResponse>(responseContent);
        }

        public RestResponse Get(string resource)
        {
            var request = new RestRequest(resource, Method.Get);
            var response = client.Execute(request);
            Console.WriteLine(response.StatusCode);
            return response;
        }
    }
}
