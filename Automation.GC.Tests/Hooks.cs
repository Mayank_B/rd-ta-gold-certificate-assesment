﻿using Allure.Net.Commons;
using OpenQA.Selenium;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.GC.Tests
{
    [Binding]
    public class Hooks
    {
        private readonly ScenarioContext scenarioContext;
        private IWebDriver driver;
        private RestClient client;

        public Hooks(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
        }


        public static AllureLifecycle allure = AllureLifecycle.Instance;
        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            allure.CleanupResultDirectory();
        }
        [BeforeScenario("UiTesting")]
        public void BeforeUiScenario()
        {
            driver = new Driver().GetDriver("chrome");
            scenarioContext["WEBDRIVER"] = driver;
        }

        [AfterScenario("UiTesting")]
        public void AfterUiScenario()
        {
            driver?.Dispose();
            driver?.Quit();
        }

        [BeforeScenario("ApiTesting")]
        public void BeforeApiScenario()
        {
            client = Setup.ClientInit();
            scenarioContext.Set(client, "Client");
        }

        [AfterScenario("ApiTesting")]
        public void AfterApiScenario()
        {
            client?.Dispose();
        }

    }
}
