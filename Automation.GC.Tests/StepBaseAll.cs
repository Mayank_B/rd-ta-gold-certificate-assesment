﻿using OpenQA.Selenium;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.GC.Tests
{
    public class StepsBaseUI
    {
        protected IWebDriver driver { get; private set; }

        protected StepsBaseUI(ScenarioContext scenarioContext)
        {
            this.driver = scenarioContext.Get<IWebDriver>("WEBDRIVER");
            this.driver.Manage().Window.Maximize();
        }

        ~StepsBaseUI()
        {
            this.driver.Quit();
        }
    }

    public class StepsBaseApi
    {
        protected RestClient client { get; private set; }

        protected StepsBaseApi(ScenarioContext scenarioContext)
        {
            this.client = scenarioContext.Get<RestClient>("Client");
        }
    }
}
