﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.GC.Tests.Model
{
    public class BookResponse
    {
        public List<Book> books { get; set; }
    }
    public class Book
    {
        public string isbn { get; set; }
        public string title { get; set; }
        public string subtitle { get; set; }
        public string author { get; set; }
        public string published { get; set; }
        public string publisher { get; set; }
        public string pages { get; set; }
        public string description { get; set; }
        public string website { get; set; }
    }
}
