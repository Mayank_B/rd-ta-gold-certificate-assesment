﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.GC.Tests.Model
{
    public class PostRequest
    {
        public string userName { get; set; }
        public string password { get; set; }
    }
}
